import logging
import os
import socket
import sys
import tqdm
from halo import Halo

SEPARATOR = "|||"
BUFFER_SIZE = 10240
MAGIC_IDENTIFIER = 'suft'

LOGLEVEL = 'INFO'

logging.basicConfig(
    level=LOGLEVEL,
    format="[%(asctime)s - %(levelname)s] [%(name)s:%(lineno)s - %(module)s.%(funcName)25s()] %(message)s"
)

try:  # tries to open a socket
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # opens a UDP socket
except socket.error:  # handles the exception if socket can't be opened
    logging.error("Failed to create socket")
    sys.exit(1)  # exits the program

try:  # tries to capture the IP and port
    host = sys.argv[1]  # IP (here it is loopback)
    port = int(sys.argv[2])  # port number
except IndexError:  # handles the exception if arguments are not entered
    logging.error("Enter the IP address and the Port Number - Please Re-run the program")
    sys.exit(2)


@Halo(text='Command = get', spinner='layer')
def getfile():
    logging.info("Fetching File...")
    servaddr = (host, port)
    filename = msg_ic[1]
    completemsgclient = (MAGIC_IDENTIFIER + SEPARATOR + msg_ic[0] + SEPARATOR + filename).encode("utf8")
    s.sendto(completemsgclient, servaddr)  # message is send to the server

    data, servaddr = s.recvfrom(BUFFER_SIZE)
    if (data.decode('utf8') == "FILE_NOT_FOUND"):
        logging.error("%s not found. Re-run the program", filename)
    else:
        finaldecodedmsgserver = data.decode('utf8').split(SEPARATOR)
        filesize = int(finaldecodedmsgserver[1])
        filename = "received_" + filename
        logging.info("Fetching File %s, size=%s...", filename, filesize)

        # sends control back to the server to start the transfer
        s.sendto((MAGIC_IDENTIFIER + SEPARATOR + "READY_TO_RECEIVED").encode("utf8"), servaddr)

        progress = tqdm.tqdm(range(filesize), f"Receiving {filename}", unit="B", unit_scale=True, unit_divisor=1024)
        with open(filename, "wb") as f:
            total_buffer_read = 0
            for _ in progress:
                data, servaddr = s.recvfrom(BUFFER_SIZE)  # receives the data from client
                total_buffer_read += len(data)
                if not data:
                    # nothing is received
                    # file transmitting is done
                    break
                f.write(data)  # writes the data to the file
                s.sendto((MAGIC_IDENTIFIER + SEPARATOR + 'NEXT_CHUNK').encode("utf8"),
                         servaddr)  # requests client for next chunk of data
                progress.update(len(data))
                if total_buffer_read >= filesize:
                    break


@Halo(text='Command = put', spinner='layer')
def putfile():  # send a file from client to the server
    logging.info("Sending File...")
    servaddr = (host, port)
    filename = msg_ic[1]

    try:  # tries to open a file in binary format
        filehandle = open(filename, "rb")
    except (FileNotFoundError):  # handles the exception if the file doesn't exist
        logging.error('%s cannot be created - Exit server', filename)
        sys.exit(10)

    filesize = os.path.getsize(filename)  # gets the filesize
    completemsgclient = (
                MAGIC_IDENTIFIER + SEPARATOR + msg_ic[0] + SEPARATOR + filename + SEPARATOR + str(filesize)).encode(
        "utf8")  # command to the server
    s.sendto(completemsgclient, servaddr)  # sends the command to the server

    data, servaddr = s.recvfrom(BUFFER_SIZE)  # control is back  to the client to start the file transfer
    if data.decode('utf8') != 'READY_TO_RECEIVED':
        logging.error("Server not ready: %s", data)
    else:
        progress = tqdm.tqdm(range(filesize), f"Sending {filename} to [{host}:{port}]", unit="B", unit_scale=True,
                             unit_divisor=1024)
        for _ in progress:
            bytes_read = filehandle.read(BUFFER_SIZE)
            if not bytes_read:
                # file transmitting is done
                break
            s.sendto(bytes_read, servaddr)  # sends the read data to the server
            data, servaddr = s.recvfrom(BUFFER_SIZE)  # receives the server request for more data
            data = data.decode("utf8")
            if data != 'NEXT_CHUNK':
                logging.error("Bad reception from server: %s", data)
                break
            progress.update(len(bytes_read))

    filehandle.close()


@Halo(text='Command = list', spinner='layer')
def listfiles():  # to list the files in the server's directory
    servaddr = (host, port)  # server address
    s.sendto((MAGIC_IDENTIFIER + SEPARATOR + msg_ic[0]).encode(), servaddr)  # sends the command to the server
    data, servaddr = s.recvfrom(BUFFER_SIZE)  # receives the string of files from server
    files = data.decode('utf8')
    fileinlist = files.split(str(SEPARATOR))  # splits the string into a list again
    logging.info('Received files list...')
    for x in fileinlist:  # prints the list of files
        logging.info(x)


@Halo(text='Command = exit', spinner='layer')
def exitserver():  # exits the server
    servaddr = (host, port)
    logging.info(
        "You just executed the exit command on the server. You can't continue the communication further. Exiting the client..")
    s.sendto((MAGIC_IDENTIFIER + SEPARATOR + msg_ic[0]).encode('utf8'), servaddr)  # sends the exit command to the server
    s.close()  # closes the socket
    sys.exit(0)  # exits the client


while (1):
    try:
        logging.info(
            "\n###############################\nEnter one of the following commands:\nget [file_name]\nput [file_name]\nlist\nexit")
        client_input = input("Enter command here -> ")  # makes the client enter a command
        msg_ic = client_input.split()  # name of the command
        msg_ic[0] = msg_ic[0].lower()
        if (msg_ic[0] == "get"):
            getfile()
        elif (msg_ic[0] == "put"):
            putfile()
        elif (msg_ic[0] == "list"):
            listfiles()
        elif (msg_ic[0] == "exit"):
            exitserver()
        else:
            logging.error("Please enter the correct command")
    except IndexError:
        logging.error("Please enter the correct command")
        pass
    except ConnectionResetError:  # handles the exception when the port number on client annd server don't match
        logging.error("Connection Failed: Enter the correct port number/IP. Plesae re-run the program.")
        sys.exit(3)
