import logging
import os
import socket
import sys
import time
import random

from scapy.layers import dns

import tqdm
from halo import Halo

SRV_DIRECTORY_EXPOSE = '/home/bast/tmp'
SEPARATOR = '|||'
BUFFER_SIZE = 10240
MAGIC_IDENTIFIER = 'suft'

LOGLEVEL = 'INFO'

logging.basicConfig(
    level=LOGLEVEL,
    format="[%(asctime)s - %(levelname)s] [%(name)s:%(lineno)s - %(module)s.%(funcName)25s()] %(message)s"
)

try:
    host = sys.argv[1]
    port = int(sys.argv[2])  # tries to input a port number to open a socket
except IndexError:
    sys.exit(1)

try:
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # open a socket
except socket.error:  # handles the exception when socket fails to create
    logging.error("Failed to create socket")
    sys.exit(2)

s.bind((host, port))  # binds the port and any IP to the socket


@Halo(text='Server command = get', spinner='layer')
def command_get(client_addr):  # this function sends data to the client
    logging.info("Sending File...")
    filename = os.path.join(SRV_DIRECTORY_EXPOSE, finaldecodedmsgclient[2])
    try:
        filehandle = open(filename, "rb")
        filesize = os.path.getsize(filename)
        completemsgclient = (filename + SEPARATOR + str(filesize)).encode('utf8')
        s.sendto(completemsgclient, client_addr)

        data, client_addr = s.recvfrom(BUFFER_SIZE)  # receives data from the client
        if data.decode('utf8') == MAGIC_IDENTIFIER + SEPARATOR + 'READY_TO_RECEIVED':
            progress = tqdm.tqdm(range(filesize), f"Sending {filename}", unit="B", unit_scale=True, unit_divisor=1024)
            for _ in progress:
                bytes_read = filehandle.read(BUFFER_SIZE)
                if not bytes_read:
                    # file transmitting is done
                    break
                s.sendto(bytes_read, client_addr)
                data, client_addr = s.recvfrom(BUFFER_SIZE)  # gets client request for the next chunk
                if data.decode('utf8') != MAGIC_IDENTIFIER + SEPARATOR + 'NEXT_CHUNK':
                    logging.error("Bad reception from client: %s", data)
                progress.update(len(bytes_read))
        filehandle.close()
    except (FileNotFoundError):
        logging.error("%s not found", filename)
        s.sendto("FILE_NOT_FOUND".encode('utf8'), client_addr)


@Halo(text='Server command = put', spinner='layer')
def command_put(client_addr):
    logging.info("Receiving File...")

    filename = finaldecodedmsgclient[2]
    filename = os.path.basename(filename)
    filesize = int(finaldecodedmsgclient[3])
    logging.info(f"Remote [{client_addr}] is sending metadata: {filename}, size={filesize}")

    # sends control back to the client to start the transfer
    s.sendto("READY_TO_RECEIVED".encode("utf8"), client_addr)

    progress = tqdm.tqdm(range(filesize), f"Receiving {filename}", unit="B", unit_scale=True, unit_divisor=1024)
    with open(filename, "wb") as f:
        total_buffer_read = 0
        for _ in progress:
            data, clientaddr = s.recvfrom(BUFFER_SIZE)  # receives the data from client
            total_buffer_read += len(data)
            if not data:
                # nothing is received
                # file transmitting is done
                break
            f.write(data)  # writes the data to the file
            s.sendto('NEXT_CHUNK'.encode("utf8"), clientaddr)  # requests client for next chunk of data
            progress.update(len(data))
            if total_buffer_read >= filesize:
                break


@Halo(text='Server command = list', spinner='layer')
def command_list(client_addr):  # lists the files in the server's directory
    path = SRV_DIRECTORY_EXPOSE
    listoffiles = (os.listdir(path))
    files = SEPARATOR.join(listoffiles).encode('utf8')  # converts the list to a string
    # print(files)
    logging.info("Sending Files List...")
    s.sendto(files, client_addr)  # sends files as a string to the server


@Halo(text='Server command = exit', spinner='layer')
def command_exit():  # function to exit the server
    logging.info("Exiting...")
    time.sleep(2)  # Gives a two second pause before exiting the server
    s.close()  # closes the socket
    sys.exit(0)


def random_ip():
    return '172.' + '.'.join([str(random.randint(1, 254)) for i in range(3)])


def dns_response(dns_data, client_address):
    ip_address = random_ip()
    pkt = dns.DNS(dns_data)
    response = bytes(dns.DNS(
        id=random.randint(0, 10000),
        qd=pkt[dns.DNS].qd,
        aa=1,
        qr=1,
        an=dns.DNSRR(rrname=pkt[dns.DNS].qd.qname, ttl=100, rdata=ip_address)
    ))
    s.sendto(response, client_address)


while (1):
    spinner = Halo(text='Waiting on [' + host + ':' + str(port) + ']', spinner='simpleDotsScrolling')
    spinner.start()
    data, clientaddr = s.recvfrom(BUFFER_SIZE)  # receives the command from the client
    try:
        pure_data = data.decode("utf8")
        if pure_data.startswith(MAGIC_IDENTIFIER + SEPARATOR):
            finaldecodedmsgclient = pure_data.split(SEPARATOR)
            commandfromclient = finaldecodedmsgclient[1]
            spinner.stop()
            if commandfromclient == "get":
                command_get(clientaddr)
            elif commandfromclient == "put":
                command_put(clientaddr)
            elif commandfromclient == "list":
                command_list(clientaddr)
            elif commandfromclient == "exit":
                command_exit()
        else:
            dns_response(data, clientaddr)
    except UnicodeDecodeError:
        pass
